function myspectrogram(x,m)
% HELPER FUNCTION TO plotSpectrogram

% Computes the sprectrogram of signal
%   Takes signal 'x', breaks it into 'nt' blocks of length 'm', 
%   computes fft of each block
%   and stores the result in xm
%   8192 - the sampling frequency

%  inputs: x = signal (sound)
%		   m = block size (number of samples to break into)
%  outputs: 

% Pad x to be a multiple of 'm'
lx = length(x);
nt = ceil(lx/m);
xp = zeros(1, nt*m); 
xp(1:lx) = x;  %xp is now x with zeros out to an integer multiple of m

%task 3: make overlaping pieces so hann window doesn't lose information
xo = xp(ceil(m/2):length(xp) - ceil(m/2)-1); %overlapping regions

xm = reshape(xp, m, nt);  %breaks xp columnwise into a matrix
xom = reshape(xo, m, nt-1); %overlapped vector split into sections
xm3 = [];

for i = 1:nt-1
    xm3 = horzcat(xm3,xm(:,i));
    xm3 = horzcat(xm3,xom(:,i));
end

xm3 = horzcat(xm3, xm(:,nt));

%task 2: use hann window instead of rectangular for the fft:
xmw = xm3.*(hann(m)*ones(1,2*nt - 1));

f1 = [0:(m/2-1)]*8192/m; %#ok<*NBRAK>
t1 = [1:2*nt]*m/(2*8192);
xmf = fft(xmw);  %computes fft of each column of the matrix xmw

sgplot(t1, f1, xmf(1:m/2,:));

end

