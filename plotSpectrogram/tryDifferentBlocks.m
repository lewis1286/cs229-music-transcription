function tryDifferentBlocks(path, file)
%% calls myspectrogram for several different lengths of input samples (m)
% input: 'path/to/file'
%        'filename.extension'

    m = [128 256 512 1024];

    for i = 1:4
        subplot(2,2,i)
        plotSpectrogram(path, file, m(i));
        title(strcat('m = ', int2str(m(i))));
    end
end
