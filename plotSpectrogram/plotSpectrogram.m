function plotSpectrogram(path, file, m)
% Plots a spectrogram with a good block size of your own file
% inputs: path - 'path/to/file/'
%		  file - 'nameOfFile.extension'
%         m    - number of samples to block in the time domain

addpath(path);

[x, Fs] =  audioread(file);

%resample to 8192 fs if needed
if Fs ~= 8192
    [p,q] = rat(8192/Fs);
    x = resample(x, p, q);
end
   
% tryDifferentBlocks(x);

myspectrogram(x, m);
title(strcat(file, 'm = ', int2str(m)));