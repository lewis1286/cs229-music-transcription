function plotFFTofSoundFile( path, file )
%Plots the time and frequency domains of an input .wav (maybe .mp3 too)
%   
% inputs: path = '/path/to/file'
%         file = 'filename.extension'

addpath( path );
% y is sample values
% Fs is sampling frequency (property of .wav files)
[y, Fs] =  audioread( file );

Nsamps = length(y);
t = (1/Fs)*(1:Nsamps);          %Prepare time data for plot

%Do Fourier Transform
y_fft = abs(fft(y));            %Retain Magnitude
y_fft = y_fft(1:Nsamps/2);      %Discard Half of Points
f = Fs*(0:Nsamps/2-1)/Nsamps;   %Prepare freq data for plot

%Plot Sound File in Time Domain
figure
plot(t, y)
xlabel('Time (s)')
ylabel('Amplitude')
title(strcat('Time signal of ', file))

%Plot Sound File in Frequency Domain
figure
plot(f, y_fft)
xlim([0 4000])
xlabel('Frequency (Hz)')
ylabel('Amplitude')
title(strcat('Frequency Response of ',file))


end

